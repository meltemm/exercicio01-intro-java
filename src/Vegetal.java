
public class Vegetal extends SerVivo{
	private String clorofila;
	private Double atp;
	
	public String getClorofila() {
		return clorofila;
	}
	public void setClorofila(String clorofila) {
		this.clorofila = clorofila;
	}
	public Double getAtp() {
		return atp;
	}
	public void setAtp(Double atp) {
		this.atp = atp;
	}
	
	
}
