
public class Monera extends SerVivo{
	private String tipoMetabolismo;
	private String tipoBacteria;
	public String getTipoMetabolismo() {
		return tipoMetabolismo;
	}
	public void setTipoMetabolismo(String tipoMetabolismo) {
		this.tipoMetabolismo = tipoMetabolismo;
	}
	public String getTipoBacteria() {
		return tipoBacteria;
	}
	public void setTipoBacteria(String tipoBacteria) {
		this.tipoBacteria = tipoBacteria;
	}
	
	
}
