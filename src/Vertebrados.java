
public class Vertebrados extends Animal{
	private String tipoEsqueleto;
	private String sistemaNervoso;
	public String getTipoEsqueleto() {
		return tipoEsqueleto;
	}
	public void setTipoEsqueleto(String tipoEsqueleto) {
		this.tipoEsqueleto = tipoEsqueleto;
	}
	public String getSistemaNervoso() {
		return sistemaNervoso;
	}
	public void setSistemaNervoso(String sistemaNervoso) {
		this.sistemaNervoso = sistemaNervoso;
	}
	
	
}
